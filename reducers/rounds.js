import { END_ROUND, START_ROUND, ADD_USER_TO_ROUND, LOGIN, UPDATE_USERS } from '../constants/ActionTypes'
const startState = {
    current_screen: 'LOGIN',
    user: null, // user id, get user details from users array
    users: [ ], // array of user objects {id, username, rounds}
    winner: null
}


export default function rounds(state = startState, action) {
  switch (action.type) {
  case LOGIN:
      return Object.assign({}, state, {user: action.user, users: state.users.concat(action.user), current_screen: 'PROFILE'})
  case START_ROUND:
      return Object.assign({}, state, {current_screen: 'ROUND'})
  case UPDATE_USERS:
      return Object.assign({}, state, {users: action.users})
  case END_ROUND:
      return Object.assign({}, state, {winner: action.winner, current_screen: 'PROFILE', users: action.users})
  default:
      return state
  }
}
