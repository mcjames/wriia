import { combineReducers } from 'redux'
import rounds from './rounds'

const rootReducer = combineReducers({
    rounds
})

export default rootReducer
