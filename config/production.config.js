var webpack = require('webpack');

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: ['./index'],
  plugins: [
    new webpack.optimize.UglifyJsPlugin({ minimize: true }),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ]
};
