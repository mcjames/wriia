import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'

import {persistStore, autoRehydrate} from 'redux-persist'

export default function configureStore(preloadedState) {
    const store = createStore(rootReducer, window.devToolsExtension && window.devToolsExtension(),  autoRehydrate());
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
