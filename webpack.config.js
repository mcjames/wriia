var path = require('path')
var configVars = require('./config/' + (process.env.NODE_ENV || 'dev') + '.config.js');

module.exports = {
  devtool: configVars.devtool,
  entry: configVars.entry,
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: configVars.plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel' ],
        exclude: /node_modules/,
        include: __dirname
      },
      {
        test: /\.css?$/,
        loaders: [ 'style', 'raw' ],
        include: __dirname
      }
    ]
  }
}

