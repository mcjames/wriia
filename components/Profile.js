import React, { PropTypes, Component } from 'react'
import {connect} from 'react-redux'
import * as RoundActions from '../actions'

import { Router, Route, IndexRoute, hashHistory, IndexLink, Link} from "react-router";
import { browserHistory } from 'react-router'
import { withRouter } from 'react-router'

import {Button, Card, Row, Col} from 'react-materialize';


class Profile extends Component {

    nextRound(){
	var round_message = RoundActions.startRound();

	this.props.dispatch(round_message);
	this.props.history.push('/round')
	
    }

    user(){
	if(this.props.winner == null){ return this.props.user}
	else{ return this.props.winner }
    }

    pageHead(){
	if(this.props.winner == null){
	    return(
		    <h1 className="center-align"> Welcome {this.user().username}</h1> 
	    )
	}
	else{
	    return(
		    <p className="center-align" ><b>Whose round is it anyway?</b></p> 
	    )
	}
    }
    
    welcomMessage(){
	if(this.props.winner == null){
	    return(<span></span>
	    )
	}
	else{
	    return(
		    <p className="center-align" ><b> {this.user().username}</b> its your round!!!</p> 
	    )
	}
    }
    render() {
	    return (

		    <Row >
		    
		    <Row>
		    <Col offset="s3" s={6} > 
		    {this.pageHead()}
		</Col>
		    </Row>
		    
		    <Row>
		    <Col offset="s3" s={6}>
		    <img  src={this.user().pic} width='100px' height='100px' />
		    </Col>
		    </Row>

		    <Row>
		    <Col offset="s3" s={6}>
		    {this.welcomMessage()}
		    </Col>
		    </Row>
		    
		    <Row>
		    <Col offset="s3" s={6}>
		    <Button  onTap={this.nextRound.bind(this)} onClick={this.nextRound.bind(this)}  style={{float:'right', width: '100%',cursor: 'pointer'}} > Next Round  </Button>
		    </Col>
		    </Row>

		</Row>
	    )
  }
}

const mapStateToProps = (state) => {
    return {
	user: state.rounds.user,
	current_screen: state.rounds.current_screen,
	winner: state.rounds.winner
    }
}


export default connect(mapStateToProps)(Profile);
