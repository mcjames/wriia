import React, { PropTypes, Component } from 'react'
import {connect} from 'react-redux'
import { DropTarget } from 'react-dnd';
import * as RoundActions from '../actions'
import * as ActionTypes from '../constants/ActionTypes'
import User from './User'

import { Button } from 'react-bootstrap';

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

const partyTarget = {
    drop(props) {
    }
};

function collect(connect, monitor) {
    return {
	connectDropTarget: connect.dropTarget(),
	isOver: monitor.isOver(),
	item: monitor.getItem()
    };
}

class Party extends Component {    
    render() {
	const { connectDropTarget, isOver, item } = this.props;
	
	var rendered_users = this.props.users.map(function(given_user){
	    return(<User key={given_user.id} user={given_user}/>);
	});

		<Col xs={6} md={4} style={{  float: this.props.side}}>
		
		<h3> Your "friends" </h3>
		{rendered_users}	
	    </Col>

	var sizecon = this.props.users.length < 3
	return connectDropTarget(
	    <div style={{ height: sizecon ? '400px' : null}}>
		<h3> In round? </h3>
		{rendered_users}
		</div>
	       ) 	   
    }
}


Party.propTypes = {
    users: PropTypes.array.isRequired,
    side: PropTypes.string.isRequired
}

export default DropTarget(ActionTypes.USER, partyTarget, collect)(Party);

//export default Party;
