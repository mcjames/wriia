import React, { Component, PropTypes } from 'react'
import {connect} from 'react-redux'
import * as RoundActions from '../actions'
import { DragSource } from 'react-dnd';
import * as ActionTypes from '../constants/ActionTypes'
import {Button, Card, Row, Col} from 'react-materialize';

const userSource = {
    beginDrag(props) {
	console.log("start dragging")
	return {
	    user: props.user
	};
    },

    endDrag(props, monitor, component) {
	var user = props.user;
	var gdispatch = props.dispatch;
	
	if(monitor.didDrop()){
	    fetch('/join_round', {
		method: 'post',
		headers: {
		},
		body: JSON.stringify(user)
	    }).then(function(response){ return response.json()}).then(function(users){
		var update_users_message = RoundActions.updateUsers(users);
		gdispatch(update_users_message);
	    });
	}
    }
    
};

function collect(connect, monitor) {
    return {
	connectDragSource: connect.dragSource(),
	isDragging: monitor.isDragging()
    };
}

class User extends Component {

    componentDidMount() {
	console.log("mounted")
	// the following line won't be bound to the store here...

	const { actions } = this.props;
//	div.addEventListener('click', function() {}, false );
	document.addEventListener("tap", this.toggleRound );
    }
    
    toggleRound(){
	console.log('toggleRound');
	var gdispatch = this.props.dispatch;
	var user = this.props.user;
	var url_to_hit = null;
	if(true == user.in_round){
	    url_to_hit = '/leave_round'
	}
	else{
	    url_to_hit = '/join_round'
	}

		
	console.log(url_to_hit);
	fetch(url_to_hit, {
	    method: 'post',
	    headers: {
	    },
	    body: JSON.stringify(user)
	}).then(function(response){ return response.json()}).then(function(users){
	    var update_users_message = RoundActions.updateUsers(users);
	    gdispatch(update_users_message);
	});
    }


   
    render() {
	const { connectDragSource, isDragging, user } = this.props;
	return connectDragSource(
	    
			<div addEventListener={this.toggleRound.bind(this)}
				      style={{cursor: 'pointer',  border: '1px solid black', width: '100%', height: '51px', backgroundColor: 'lightblue' }}
	    onTap={this.toggleRound.bind(this)}
	    onDoubleClick={this.toggleRound.bind(this)}
		>
		<div style={{cursor: 'pointer'}} onTap={this.toggleRound.bind(this)}>
		
		<img src={this.props.user.pic} width='50px' height='50px' style={{float: 'right'}}  />
		<img src="/img/pint.jpg" width='50px' height='50px' style={{float: 'right'}}  />
		<div style={{float: 'right'}} ><b>  {this.props.user.rounds}x</b> </div>
		
		<span className="flow-text"> <small>{this.props.user.username}</small></span>
		</div>
		</div>
		
	);
    }
}

User.propTypes = {
    user: PropTypes.object.isRequired,
    // Injected by React DnD:
    isDragging: PropTypes.bool.isRequired,
    connectDragSource: PropTypes.func.isRequired
}


export default connect()(DragSource(ActionTypes.USER, userSource, collect)(User));

//export default connect(mapStateToProps)(User);
