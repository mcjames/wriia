import React, { PropTypes, Component } from 'react'
import {connect} from 'react-redux'
import * as RoundActions from '../actions'
import Party from './Party'
import LameParty from './LameParty'


import { Router, Route, IndexRoute, hashHistory, IndexLink, Link} from "react-router";
import { browserHistory } from 'react-router'
import { withRouter } from 'react-router'


import {Button, Card, Row, Col} from 'react-materialize';

class Round extends Component {

    constructor(props) {
	super(props);
	this.getUsers();	
    }
    
    calculateRound(){
	if(this.props.users_in_round.length > 1){
	    var gprops = this.props;
	    var gdispatch = this.props.dispatch;
	    var users = this.props.users_in_round;
	    fetch('/calculate_round', {
		method: 'post',
		headers: {},
		body: JSON.stringify(users)
	    }).then(function(response){ return response.json()}).then(
		function(winner_users){
		    gdispatch(RoundActions.endRound(winner_users.winner, winner_users.users));
		    gprops.history.push('/profile')

			     });
	}	
    }

    getUsers(){
	var gdispatch = this.props.dispatch;
	
	fetch('/users').then(function(response){ return response.json()}).then(function(gusers){

	    var up_users_message = RoundActions.updateUsers(gusers);
	    console.log(up_users_message);
	    gdispatch(up_users_message);

	});

    }

    
    render() {
	    var sizecon = this.props.users_in_round.length < 5
	    var sizecon2 = this.props.users_not_in_round.length < 5
	    
	    return (
		    <Row>
		    <Col s={6} style={{ border: '1px solid black', height: sizecon2 ? '500px' : null, backgroundColor: 'skyblue', float: 'left'}}>
					      <LameParty key='left'  users={this.props.users_not_in_round} side="left" />
	    	    <Button bsStyle="success" bsSize="large" onClick={this.getUsers.bind(this)} onTap={this.getUsers.bind(this)} style={{cursor: 'pointer', float:'right', width: '100%'}} >Find Friends</Button>
		    </Col>

		    <Col s={6} style={{  float: 'right', border: '1px solid black', height: sizecon ? '500px' : null, backgroundColor: 'skyblue' }} >
		    <Party key='right'  users={this.props.users_in_round} side="right" />
	    	    <Button bsStyle="success" bsSize="large" onClick={this.calculateRound.bind(this)} onTap={this.calculateRound.bind(this)} style={{cursor: 'pointer',float:'right', width: '100%'}} >Who gets the round?  </Button>
		    </Col>
		</Row>
		   ) 	   
  }
}


const mapStateToProps = (state) => {
    var inRound = function(user){
	return !!user.in_round;
    }

    var notInRound = function(user){
	return !user.in_round;
    }

    return {
	current_user: state.rounds.user,
	users: state.rounds.users,
	users_in_round: state.rounds.users.filter(inRound),
	users_not_in_round: state.rounds.users.filter(notInRound),
	current_screen: state.rounds.current_screen
    }
}


export default connect(mapStateToProps)(Round);
