import React, { PropTypes, Component } from 'react'
import {connect} from 'react-redux'
import { DropTarget } from 'react-dnd';
import * as RoundActions from '../actions'
import * as ActionTypes from '../constants/ActionTypes'
import User from './User'

import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import { Button } from 'react-bootstrap';

class LameParty extends Component {    
    render() {
	
	var rendered_users = this.props.users.map(function(given_user){
	    return(<User key={given_user.id} user={given_user}/>);
	});
	var sizecon = this.props.users.length < 3
	//border: '1px solid black', width: '49%', height: '600px',
	return(
	    <div>
		<h3> Your "friends" </h3>
		{rendered_users}
	    </div>
	) 	   
    }
}


LameParty.propTypes = {
    users: PropTypes.array.isRequired,
    side: PropTypes.string.isRequired
}

export default LameParty;
