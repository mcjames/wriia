import React, { PropTypes, Component } from 'react'
import {connect} from 'react-redux'
//import { Button } from 'react-bootstrap';
//import { FormControl }  from 'react-bootstrap';

import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import {deepOrange500} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import { Router, Route, IndexRoute, hashHistory, IndexLink, Link} from "react-router";
import { browserHistory } from 'react-router'
import { withRouter } from 'react-router'

import {Button, Card, Row, Col} from 'react-materialize';

import * as RoundActions from '../actions'


class Login extends Component {

    
    loginUser(){
	var gdispatch = this.props.dispatch;
	console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	console.log(this.props)
	console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	var mprops = this.props
	fetch('/signup', {
	    method: 'post',
	    headers: {
	    },
	    body: JSON.stringify({username: this.state.username, pic: this.state.pic})
	}).then(function(response){ return response.json()}).then(function(j){

	    var login_message = RoundActions.login(j);
	    console.log(login_message);
	    gdispatch(login_message);
	    mprops.history.push('/profile')
	});

    }

    handleUsernameChange(e){
	this.setState({username: e.target.value});
    }

    handlePicChange(e){
	this.setState({pic: e.target.value});
    }
    
    handlePasswordChange(e){
	console.log('we dont actually use the password')
    }
    
    render(){
	    return (
		    
		    <Row >
		    <Col s={4} >&nbsp;</Col>
		    <Col s={4}>
		<input type="text" placeholder="Username" onChange={this.handleUsernameChange.bind(this)} />
		<input type="text" placeholder="Profile pic url" onChange={this.handlePicChange.bind(this)} /> 
		<input type="password" placeholder="Password" onChange={this.handlePasswordChange.bind(this)} />
		    <Button onTap={this.loginUser.bind(this)} onClick={this.loginUser.bind(this)} style={{cursor: 'pointer'}} >Login </Button>
		    </Col>
		    </Row>
	)	    
    }
}

const mapStateToProps = (state) => {
    var selected_user_id = null;
    if(state.rounds.user != null){
	selected_user_id = state.rounds.user.id;
    }
    
    return {
	user: state.rounds.user,
	user_id: selected_user_id,
	current_screen: state.rounds.current_screen
    }
}


export default connect(mapStateToProps)(Login);
