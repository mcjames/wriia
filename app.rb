require 'sinatra'
require 'json'
require 'mongo'

configure do
  client = Mongo::Client.new(ENV['MONGODB_URI'])
  db = client.database
  set :mongo_db, client[:users]
end

helpers do
  def get_users
    res = settings.mongo_db.find({ 'uid' => 1 }).to_a.first
    if res.nil?
      {}
    else
      res['users']
    end
  end

  def up_users(users)
    if settings.mongo_db.find({'uid' => 1}).to_a.first.nil?
      settings.mongo_db.insert_one({'uid' => 1, 'users' => {}})
      up_users(users)
    else
      settings.mongo_db.update_one( {'uid' => 1}, {'uid' => 1, 'users' => users})
    end
  end
end

set :public_folder, './'

post '/signup' do
  content_type :json
  begin
    payload = JSON.parse(request.body.read) # validate provided json
    username = payload['username']
    id = Random.rand(1..1000000)
    users = get_users
    users[id.to_s]= {'rounds'=> 0, 'id'=> id , 'username'=> username, 'pic'=> payload['pic'], 'in_round' => false}
    up_users(users)
    users[id.to_s].to_json
  end
end

get '/' do
  File.read(File.join('.', 'index.html'))
end

get '/users' do
  content_type :json
  get_users.values.to_json
end


post '/leave_round' do
  content_type :json
  begin
    user = JSON.parse(request.body.read) # validate provided json
    users = get_users
    users[user['id'].to_s]['in_round']=false
    up_users(users)
    users.values.to_json    
  end
end

post '/join_round' do
  content_type :json
  begin
    user = JSON.parse(request.body.read) # validate provided json
    users = get_users
    users[user['id'].to_s]['in_round']=true
    up_users(users)
    users.values.to_json    
  end
end

def find_winner(round)
  winner = nil
  round.each do |user|
    if winner.nil?
      winner = user
    end

    if winner['rounds'] < user['rounds']
        winner = user
    end      
  end
  winner
end

def find_others(round, winner)
  round.select{|user| user['id'] != winner['id']}
end

# calculates the winner, updates stats, resets the round
post '/calculate_round' do
  content_type :json
  
  begin
    round = JSON.parse(request.body.read) # validate provided json
    winner = find_winner(round)
    others = find_others(round, winner)
    
    #we have the winner, time to update
    winner['rounds'] -= (round.length - 1)
    winner['in_round'] = false
    users = get_users
    users[winner['id'].to_s] = winner
    
    others.each do |user|
      user['rounds'] += 1
      user['in_round']= false
      users[user['id'].to_s] = user
    end

    up_users(users)
    {winner: winner, users: users.values}.to_json
  end
end
