import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {dragsource} from 'react-dnd'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from "react-dnd-html5-backend";
import Login from '../components/Login'
import Profile from '../components/Profile'
import Round from '../components/Round'
import * as RoundActions from '../actions'

//import { default as TouchBackend } from 'react-dnd-touch-backend';
import TouchBackend from 'react-dnd-touch-pointer-events-backend';



import {Button, Card, Row, Col} from 'react-materialize';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

var App = React.createClass({
    
    render: function() {
	return (
		<div path="/" className="container">
		{this.props.children}

	    </div>
	);
    }
});

//export default DragDropContext(HTML5Backend)(App);
export default DragDropContext(TouchBackend)(App);
