import expect from 'expect'
import React from 'react'
import { shallow } from 'enzyme'
import App from '../containers/App'

function setup() {
    const props = {
	addTodo: expect.createSpy()
    }

    const enzymeWrapper = shallow(<App {...props} />)

    return {
	props,
	enzymeWrapper
    }
}

describe('components', () => {
    describe('App', () => {
	it('should render self and subcomponents', () => {
	    const { enzymeWrapper } = setup()

	    expect(enzymeWrapper.find('header').hasClass('header')).toBe(true)

	    expect(enzymeWrapper.find('h1').text()).toBe('todos')

	    const todoInputProps = enzymeWrapper.find('Login').props()
	    expect(todoInputProps.newTodo).toBe(true)
	    expect(todoInputProps.placeholder).toEqual('What needs to be done?')
	})

	it('should call addTodo if length of text is greater than 0', () => {
	    const { enzymeWrapper, props } = setup()
	    const input = enzymeWrapper.find('TodoTextInput')
	    input.props().onSave('')
	    expect(props.addTodo.calls.length).toBe(0)
	    input.props().onSave('Use Redux')
	    expect(props.addTodo.calls.length).toBe(1)
	})
    })
})
