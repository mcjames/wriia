import 'babel-polyfill'
import 'whatwg-fetch';
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import {persistStore} from 'redux-persist'
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import {Button, Card, Row, Col} from 'react-materialize';
import App from './containers/App'
import configureStore from './store/configureStore'

import Login from './components/Login'
import Profile from './components/Profile'
import Round from './components/Round'


const store = configureStore()
//persistStore(store)

render(

	<Provider store={store}>
	<Router history={hashHistory}>
	<Route path="/" component={App}>
	<IndexRoute component={Login}></IndexRoute>
	<Route path="profile" name="profile" component={Profile}></Route>
	<Route path="round" name="round" component={Round}></Route>
	</Route>
	</Router>
	</Provider>
    ,
    
    document.getElementById('root')
);
