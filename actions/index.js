import * as types from '../constants/ActionTypes'

export function startRound() {
    
    return { type: types.START_ROUND }
}

export function endRound(winner,users){
    return { type: types.END_ROUND, winner: winner, users: users }
}

export function updateUsers(users) {
    return { type: types.UPDATE_USERS, users: users}
}

export function login( user) {
  return  { type: types.LOGIN, user: user }
}

